# Полезный контент компании

## Стандарты
1. [Стандарты компании для фронтенда проектов.](https://gitlab.com/indep.pro/standards)


## Старт кодовой базы фронта проектов
1. [SEO-friendly сборка на базе Webpack 5 для фронта](https://gitlab.com/indep.pro/frontend-bundler) - без реактивных фреймворков/библиотек, на базе html-шаблонизатора Pug.

